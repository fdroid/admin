= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-05-18 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, Hans-Christoph Steiner, Andrew Lewman, John Sullivan,  Matthias Kirschner

=== Guests
Sylvia, Licaon Kter, uniq

https://pad.riseup.net/p/fhapnw0z88Prwa1EM5z7

F-Droid Board meeting May 18, 2023 at 16:00 UTC

Meeting Location: https://meet.calyx.net/OverallStrokesDeriveAutomatically
Previous meeting's notes: https://pad.riseup.net/p/qq2wJ5n_SqCLQIv6VLwD

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conduct: https://f-droid.org/en/docs/Code_of_Conduct/. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda

. Following up on trademark progress
.. Look at community discussions about trademark/logo
.. Notes from meeting with Pam Chestek: https://pad.riseup.net/p/lLpQdi1mVGDbYUR6oKpN
.. Contact EU lawyer (Should we register logo & wordmark concurrently?) (Andrew?)
. Follow up on response to announcement
.. FOSS & Crafts Episode (Morgan)
.... https://fossandcrafts.org/episodes/057-f-droid.html
.... https://news.ycombinator.com/item?id=35943836
. Follow up on Running budget spreadsheet to aggregate all funding sources and potential/actual spending items, will post in GitLab (somewhere TBD). Approx ~ $70k USD across all sources. (MD)  The donations are publicly listed on
.. https://opencollective.com/f-droid
.. https://opencollective.com/f-droid-euro
.. https://liberapay.com/F-Droid-Data/
. Follow up on GitLab workflows for agenda (Morgan), minutes (Andrew), etc.
. Transferring domain names to board control.
. Should F-Droid continue to post content to Twitter?
.. Inspired by X.org Foundation's recent decision (https://x.org/wiki/BoardOfDirectors/MeetingSummaries/2023/05-03/)
.. (MD's asynchronous input): FWIW, I think the general consensus from those who do the posting is to withdraw from Twitter altogether, basically following the same pattern that X.org Fdn did with similar announcements and directing people to Mastodon. Sylvia is the best person to probably address the question to as I think they are the de facto "organizer" (for lack of a better term) of that working group.
. Should we determine (and subsequently publicize) a standard process for community members to submit items for the agenda or to otherwise be discussed and/or decided by the board?
.. The current strategy is to release the agenda ahead of the meeting with the final agenda item listed as "Community Engagement" and people can propose topics there. Though we've also pulled in issues posted on the fdroid gitlab.
. In-person meeting in Aberdeen, Scotland in June (https://gitlab.com/fdroid/admin/-/issues/387)
.. Here's a poll to find the best days in June: https://framadate.org/SalsQXBuUhA3A6mX
. Community Engagement (Community members can propose agenda items below)
.. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)

=== Votes

. Morgan motion to approve January, February, March, April Meeting Minutes
.. Andrew seconds, pending attendees fix for relevant months
.. John and Matthias approve, but abstain from April due to non-attendance
.. APPROVED

. Morgan motion to approve policy that no exclusive posting on proprietary platforms
.. John seconds
.. APPROVED

== Notes on Agenda
. Following up on trademark progress
.. Look at community discussions about trademark/logo
.. Notes from meeting with Pam Chestek: https://pad.riseup.net/p/lLpQdi1mVGDbYUR6oKpN
... sent engagement letter to board@, morgan can sign the letter
... shall we do logo and wordmark together? yes
... Do we want to engage in the fight over the term "droid"?
.... (Morgan) My impulse is to stick with our current branding and see if the fight comes to us (without intentionally provoking)
.... (andrew) The term 'droid' seems to be abandoned by Verizon, Google may care about the android logo, and we're in a different market than Lucas Film, so the risk isn't too high(?)
.. Contact EU lawyer (Should we register logo & wordmark concurrently?) (Andrew?)
... working through Pam
. Follow up on response to announcement
.. FOSS & Crafts Episode (Morgan)
... https://fossandcrafts.org/episodes/057-f-droid.html
... https://news.ycombinator.com/item?id=35943836
.... went public, well received, thank you to Hans and Sylvia
. Follow up on Running budget spreadsheet to aggregate all funding sources and potential/actual spending items, will post in GitLab (somewhere TBD). Approx ~ $70k USD across all sources. (MD)  The donations are publicly listed on
... https://opencollective.com/f-droid
... https://opencollective.com/f-droid-euro
... https://liberapay.com/F-Droid-Data/
. Follow up on GitLab workflows for agenda (Morgan), minutes (Andrew), etc.
.. (mogran) agenda: visibile, no feedback from community
.. (andrew) minutes: need more feedback on process, js/mk abstain from april, all approved pending corrections in jan/feb minutes for attendees
. Transferring domain names to board control.
.. check status of accounts (f-droid.org, etc under Kieran's name, ~some others under Hans's name) registered on namecheap. Should be relatively easy to change on namecheap
.. What is the legal process to change the 'owner' of the account?
.. Role account over to board, create an 'accounts@fdroid.net' email account to switch accounts to (the Chair and technical lead should control that email account and it would be rolled over when the positions passes)
. Should F-Droid continue to post content to Twitter?
.. Inspired by X.org Foundation's recent decision (https://x.org/wiki/BoardOfDirectors/MeetingSummaries/2023/05-03/)
.. (MD's asynchronous input): FWIW, I think the general consensus from those who do the posting is to withdraw from Twitter altogether, basically following the same pattern that X.org Fdn did with similar announcements and directing people to Mastodon. Sylvia is the best person to probably address the question to as I think they are the de facto "organizer" (for lack of a better term) of that working group.
... options could be to post only announcements to twitter, make it post only and push interactions to mastodon or other free software platforms
... vote: information should not be exclusively on proprietary platforms voted and passed unanimously
. Should we determine (and subsequently publicize) a standard process for community members to submit items for the agenda or to otherwise be discussed and/or decided by the board?
.. we should document the agenda and minutes process somewhere public (docs, wiki)
.. The current strategy is to release the agenda ahead of the meeting with the final agenda item listed as "Community Engagement" and people can propose topics there. Though we've also pulled in issues posted on the fdroid gitlab.
.. Morgan will create agenda for next meeting after the board meeting. andrew will download the meeting minutes/agenda right after each meeting finishes.
. In-person meeting in Aberdeen, Scotland in June (https://gitlab.com/fdroid/admin/-/issues/387)
.. Here's a poll to find the best days in June: https://framadate.org/SalsQXBuUhA3A6mX
... According to poll, the core dates are going to be June 27-28 with a bit of wiggle room if people want to stay longer
... get flights/travel plans to hans now
. Community Engagement (Community members can propose agenda items below)
.. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)
