= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-07-20 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, Hans-Christoph Steiner,  John Sullivan, Michael Downey, Max Mehl, Matthias Kirschner

=== Guests

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnwz88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda

. Follow up from in-person meeting in Scotland
.. CoC/Community Council Team Elections (as discussed at the in person meeting)
... Set up an admin issue where candidates who are interested can post a short description of their track record and why they want to be on the team
... A Board Member to handle counting the votes as an almost anonymous process.
... Use dedicated email address to send votes to. (needs to be set up)
... Each @fdroid member sends using their well-known email account (attached to gitlab account).
... Ranked-choice voting (Tor Project has a Python script to implement this).
... One credible objection disqualifies a candidate.
... Only Reporters in @fdroid are eligible for Community Council.
... @board does not have a vote for Community Council
... Should Board members be on the Community Council? If so, limit to one.
... Final policies and procedures for CoC enforcement and training should be outlined by the Community Council Team once it is in place, presented at a future board meeting, then ratified after approval by the broader community and the board. 
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident (discussed in meeting last month)?
.. Fundraising/Crowdfunding Campaign
... Goals
... Set up committee to run a December campaign?
... Create educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience 
.. Board to take over the DNS/domain name transfer?
... Is this the same as item 6.x below?
. Status update on legal request(s) per mailing list
.. designating a point of contact?
.. SFLC India Letter Of Engagement (Michael and Hans)
... review, approval/sign-off on letter sent 223-6-8
. Following up on trademark progress 
.. Engagement letter (send to board, Morgan will sign on behalf of board, needs to be signed by commons conservancy)
. Updates on budget-tracking (MD)
.. https://monitor.f-droid.org/donations
.. Discuss proposed repo for tracking disbursement requests
. Follow up on workflows for agenda (Morgan), minutes (Andrew), community engagement etc.
.. Use the same riseup pad each month and post link somewhere public (docs, wiki)
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Approve minutes from previous meeting
. Community Engagement (Community members can propose agenda items below)
.. RFC: Client maintenance sponsored by the Calyx Institutefootenote:[https://gitlab.com/fdroid/admin/-/issues/388].
.. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?

== Votes on Agenda
. Morgan motions to approve board meeting minutes from June
.. Michael, John second
.. Approved.

== Notes on Agenda

. Follow up from in-person meeting in Scotland
.. CoC/Community Council Team Elections (as discussed at the in person meeting)
... Set up an admin issue where candidates who are interested can post a short description of their track record and why they want to be on the team
... A Board Member to handle counting the votes as an almost anonymous process.
... Use dedicated email address to send votes to. (needs to be set up)
... Each @fdroid member sends using their well-known email account (attached to gitlab account).
... Ranked-choice voting (Tor Project has a Python script to implement this).
... One credible objection disqualifies a candidate.
... Only Reporters in @fdroid are eligible for Community Council.
... @board does not have a vote for Community Council
... Should Board members be on the Community Council? If so, limit to one.
... Final policies and procedures for CoC enforcement and training should be outlined by the Community Council Team once it is in place, presented at a future board meeting, then ratified after approval by the broader community and the board. 
... Matthias: Will check with FSFE if the person who deals with their CoC procedures could sit down with us to help guide this discussion
... Michael's proposal: put out a call for interest, submit info, confidential process before we announce the election so we can see how many people we're working with
... Step : Announce timeline and private Q&A's with interested parties (~1 week). Step 1: Candidates propose themselves on GitLab (or whatever) after going through step  (~2-3 days). Step 2: Period of Q&A or expressing confidential concerns (~2 weeks) Step 3: "election" 
.... Michael will write a proposal announcement and circulate it to the board for any concerns (GUADEC is next week but he will endeavor to get the draft out by end of next week)
... John: having a quicker turnaround but a shorter term limit for initial 'election' might be useful
... John: Maybe some system where people show which candidates they support and each candadate has to hit a minimum
.... if we have 3 potential voters, and we think half of them will hopefully vote (15), and people can vote as many times as they want, we could hope for a person to get 8 votes to show confidence? 
.... Gnome has a system where people put up their candidacy and they need to get a minimum number of 'second's 
.... With moderator elections at StackExchange, each entitled voter has max 3 votes available.
... How do we make sure everyone hears about the election in spite of current communication issues (see 8.2).
.... Announce everywhere to get as much coverage as we can 
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident (discussed in meeting last month)?
... Morgan action: respond to immediate incident, and send email to board to get asynchronous feedback from board on setting up a 'committee' to facilitate as mediators in the future
.... discuss a budget for future concerns if we need to hire an outside mediator or a lawyer
.... How to deal with conflicts of interest
.. Fundraising/Crowdfunding Campaign
... Goals
... Set up committee to run a December campaign?
... Create educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience 
.. Board to take over the DNS/domain name transfer?
... Is this the same as item 6.x below?
. Status update on legal request(s) per mailing list
.. designating a point of contact?
.. SFLC India Letter Of Engagement (Michael and Hans)
... review, approval/sign-off on letter sent 223-6-8
. Following up on trademark progress 
.. Engagement letter (send to board, Morgan will sign on behalf of board, needs to be signed by commons conservancy)
. Updates on budget-tracking (MD)
.. https://monitor.f-droid.org/donations
.. Discuss proposed repo for tracking disbursement requests
.. Michael: Current financial report: $99,141.64 USD in our accounts 
. Follow up on workflows for agenda (Morgan), minutes (Andrew), community engagement etc.
.. Use the same riseup pad each month and post link somewhere public (docs, wiki)
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Approve minutes from previous meeting
.. + from Morgan, John, Michael
. Community Engagement (Community members can propose agenda items below)
.. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)
.. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
. Set up more committees to take larger topics out of main board meetings
.. CoC/elections (include Fay, Izzy, and Sylvia)
.. Fundraising committee (John, Michael, Morgan)
