= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate:  2023-04-20 16:00 UTC

== Attendees

=== Board Members
Michael Downey, Morgan Lemmer Webber, Hans-Christoph Steiner, Andrew Lewman

=== Guests
Sylvia

Meeting Location: https://meet.calyx.net/OverallStrokesDeriveAutomatically
Previous meeting's notes: https://pad.riseup.net/p/8arEeBhmw2RL7GFmST5X

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conduct: https://f-droid.org/en/docs/Code_of_Conduct/. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda
. Following up on trademark progress
.. Look at community discussions about trademark/logo
.. Notes from meeting with Pam Chestek: https://pad.riseup.net/p/lLpQdi1mVGDbYUR6oKpN
.. Contact EU lawyer (Should we register logo & wordmark concurrently?) (Andrew?)
. Follow up on response to announcement
..FOSS & Crafts Episode (Morgan)
. Follow up on Running budget spreadsheet to aggregate all funding sources and potential/actual spending items, will post in GitLab (somewhere TBD). Approx ~ $70k USD across all sources. (MD)  The donations are publicly listed on
.. https://opencollective.com/f-droid
.. https://opencollective.com/f-droid-euro
.. https://liberapay.com/F-Droid-Data/
. Follow up on GitLab access for board members (Hans)
. Community Engagement (Community members can propose agenda items below)
. Transferring domain names to board control.

=== Other proposed agenda items (requester)
	* Are we clear on the process/location/method for storing board meeting materials (agendas, minutes, etc.)? What else if anything needs to be set up on GitLab? (Michael Downey)
	* Next meeting date and cadence? (Michael Downey)
		* May 18, 2023 at 16:00 UTC (If we stick with the 3rd Thursday of the month pattern)
	* Do we want to announce this meeting like the dev meetings in the F-Droid or F-Droid Dev chats automatically? (Sylvia)


== Notes
. (trademarks)
. (post-announcement)
. Follow up on Running budget spreadsheet to aggregate all funding sources and potential/actual spending items, will post in GitLab (somewhere TBD). Approx ~ $70k USD across all sources. (MD)
.. Proposal: CSV files stored in a GitLab sub-project under board. One file for combined OpenCollective ledger, one file for Liberapay. Requests for distributions (OC) or for adjustments to takes (LP) would come through as issues for tracking and then update in ledgers and relevant website. Would need a treasurer@ email alias so MD can receive copies of LP notifications. Examples of layout/format for ledgers follow:

Date,Amount,Description,URL,Account,Status,Balance
2023-04-20,54877.02,Initial Starting Balance,https://example.com/01, OC-USD,Note,54877.02 
2023-04-20,12345.68,Initial Starting Balance,https://example.com/02, OC-EUR,Note,12345.68 
2023-04-21,(42.42),Example snacks,https://example.com/03, OC-USD,Open,54834.60 

Date,Amount,Source,Status,URL,Description
2023-W16,309.03,Donations,Conrfirmed,,Weekly donations
2023-W16,-311.94,Licaon-kter,Confirmed,,Assigned take for week 
2023-W16,0,eighthave,Request,https://example.com/10, Request for take
(Action: MD for next month)
(Set up tresurer email for liberapay notifications, Action: Sylvia or Hans)

. Follow up on GitLab access for board members
	* Andrew will upload minutes (for Jan, Feb, Mar, April) via gitlab board official, board members will approve via comment on gitlab, then Andrew will post in the Admin repo under the board folder so they're public
. Following up on trademark process
	* Andrew will follow up in the next week or two
. For next meeting
	* Sylvia will set up a bot to announce monthly meetings
	* Morgan will put agenda in a publicly available GitLab repo ~ a week before so that community can see and suggest agenda items
. Response to announcement
	* Not a lot of diversity on the board. (Maybe in January start making noise about board elections in March 2024, and we will work on diversity with turnover)
	* Overall positive responses to announcement. 
	* FOSS & Crafts episode hopefully released April 22/23 (Action: Morgan), and cross-promoted by F-Droid (Action: Hans, Sylvia)
. Transferring domain names to board control.
	* Keran is willing to do this, we just need to get a plan in place, then hold his hand through the process
	* Action: set up an F-Droid board account on namesheep and transfer process (Action: MD)
