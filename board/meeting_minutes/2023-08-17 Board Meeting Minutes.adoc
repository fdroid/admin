= F-Droid Board Meeting Minutes
F-Droid Board <board@fdroid.net>
:revdate: 2023-08-17 16:00 UTC

== Attendees

=== Board Members
Morgan Lemmer Webber, John Sullivan, Michael Downey, Andrew Lewman

=== Guests
SylvieLorxu, Licaon_Kter, Fay, Izzy, Paul, Gregor

* Meeting Locationfootnote:[https://meet.calyx.net/OverallStrokesDeriveAutomatically]
* Previous meeting's notesfootnote:[https://pad.riseup.net/p/fhapnwz88Prwa1EM5z7]

Community Engagement: This meeting is open to all members of the F-Droid community and is subject to the F-Droid Code of Conductfootnote:[https://f-droid.org/en/docs/Code_of_Conduct/]. Morgan Lemmer-Webber will moderate, so please raise your hand in the conference call to be added to the queue. If you would like to propose an agenda item, please add it under the "Community Engagement" section at the end of the agenda below.

== Agenda
                          
. CoC/Community Council Call for Volunteers
.. https://gitlab.com/fdroid/admin/-/issues/416 
.. Update on submissions
.. Discuss & agree on next steps
.. Renew interim stage of moderation where @TheLastProject and @IzzySoft have permissions in GitLab that allow them to moderate (set as temporary until we have the Community Council in place but due to expire tomorrow) https://gitlab.com/fdroid/admin/-/issues/401
. Set up subcommittees
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
. Follow up on legal request(s) per mailing list
.. SFLC India Letter of Engagement
.. Discuss how to engage w/Commons Conservancy & agreement processes
.. UK takedown
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget tracking (MD)
.. https://monitor.f-droid.org/donations
.. Discuss proposed repo for tracking disbursement requests
.. We have worked out the bugs with incoming emails & GitLab so can process requests
.. Discuss how to resolve cascading permissions problems in F-Droid GitLab top-level group.
. Approve minutes from previous meeting
. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
. Community Engagement (Community members can propose agenda items below)
.. F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions.
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
.. The original 2-month period for the temporary measure in https://gitlab.com/fdroid/admin/-/issues/401 is almost over; it seems we need to extend it a bit longer though, since the Community Council is not in place yet.

== Votes
. Morgan motions to approve earlier meeting minutes
.. Michael and John Second.
.. Approved.
. Morgan motions to approve extending permissions for Izzy and Sylvie until end of Sept.
.. Michael and John Second.
.. Approved. 

== Notes on Agenda: 

. CoC/Community Council Call for Volunteers
.. https://gitlab.com/fdroid/admin/-/issues/416 
.. Update on submissions
... Only one serious inquiry as of now
... Potentially an issue with bounceback, because there is at least one person who sent an email and didn't get a response. We will do more tests to make sure and make that known to re-submit if you didn't get a response. 
.. Discuss & agree on next steps
... We will extend the deadline and re-advertise.
... Maybe Add in that people can withdraw if it becomes too much (not a defined term)
... New timeline:
.... 1 week to re-submit
.... 28-30 board will review candidates. Board members need to give at least a nominal approval 
.... 1 week for community to give confidential feedback (same email, let them know that they will receive a reply. If they don't get a reply, ask them to let us know using the backup email address)
.... Announce new team.
.. Renew interim stage of moderation where @TheLastProject and @IzzySoft have permissions in GitLab that allow them to moderate (set as temporary until we have the Community Council in place but due to expire tomorrow) https://gitlab.com/fdroid/admin/-/issues/401 (note: only Izzy was added, but TheLastProject feels that's fine, so it's about extending Izzy)
... 
... Morgan will communicate that to Hans after the meeting
. Set up subcommittees
.. Executive Committee (for legal and other issues that come up between board meetings)
.. Procedure for board to serve as mediators for community when needed
... Q: How to handle & resolve the existing/current COC incident 
.. Fundraising/Crowdfunding Campaign
... Goals
... Create Educational materials to explain the basics of what FDroid is, why it's important, and how you can use it to a general audience
. Transferring domain names to board control.
.. Create an 'accounts@fdroid.net email (with login access for Chair and Technical Lead that will be rolled over when the position is handed over)
.. On namecheap switch accounts over from current individuals to accounts@fdroid.net
.. Check in with Hans on progress (not present in meeting)
. Follow up on legal request(s) per mailing list
.. SFLC India Letter of Engagement
... No costs at the moment. If there were, they would let us know ahead of time.
.. Discuss how to engage w/Commons Conservancy & agreement processes
... Both Andrew and Michael have had difficulty contacting anyone at commons conservancy (maybe because of summer holidays?)
... Should we maybe try to set up 'regular'communication with them (quarterly? every 6 months? Send them mail in the post if we're not able to get ahold of them)
... Fay should have Michael's phone number (will send it privately to Michael to craft a short and friendly text message)
.. UK takedown
... John reached out to EFF, some initial engagement but it quieted down
... John reached out to a lawyer, it wasn't in their wheelhouse but will look for another lawyer to reccommend. 
. Following up on trademark progress
.. Engagement letter needed to be signed by commons conservancy
. Updates on budget tracking (MD)
.. https://monitor.f-droid.org/donations
.. Discuss proposed repo for tracking disbursement requests
.. We have worked out the bugs with incoming emails & GitLab so can process requests
.. Discuss how to resolve cascading permissions problems in F-Droid GitLab top-level group.
.. Permissions: Michael suggests setting up an "Fdroid board" level project so that we can handle permissions with confidential information as a short-term solution while we find a long-term solution. Michael will write up initial findings for this and send it to the board. 
. Approve minutes from previous meeting
.. Morgan will send July minutes to Andrew (since he was out last month)
.. Proposal to not attribute personal opinions as part of dicssion -- attribute motions, seconds, action items; also to not include pad URLs because pads are dynamic
.. Andrew will set gitlab to have a deadline to remind board members to read/approve/remember that the meeting minutes exist
.. Morgan will add a link to the outstanding meeting minutes to the board meeting reminder emails
.. We will continue to approve the minutes in the next meeting
.. You don't have to attend the meeting to approve the minutes (as long as you read them)
. RFC: Client maintenance sponsored by the Calyx Institute (https://gitlab.com/fdroid/admin/-/issues/388)
. Improving internal communication from the current fragmented situation (not everyone being on matrix/XMPP/forum/mail threads/etc.), having a channel for announcements for the team & board (both public and private) that everyone has easy access to (maybe gitlab works as an interim solution). Use discourse for non-development discussions/announcements maybe?
.. Michael/Mathias have been working on setting up a Matrix that can be bridged to XMPP and IRC. Fay offered to help with IRC bridge.
. Community Engagement (Community members can propose agenda items below)
.. F-Droid board group (https://forum.f-droid.org/g/board?asc=true&order=) was created as per https://gitlab.com/fdroid/admin/-/issues/408 but many board members are still missing. Please give a forum admin (Sylvia / Licaon_Kter) your forum username
.. New Google Play policy, D-U-N-S registration for organisations. What are we going to do with the "Free Your Android" account publishing F-Droid Nearby? See https://play.google.com/store/apps/details?id=org.fdroid.nearby and https://chaos.social/@SylvieLorxu/110827167687326238 (summary). Which name will we use? Who will be responsible? Or will we let the account die? Also we may want to update https://f-droid.org/en/docs/FAQ_-_General/#why-isnt-f-droid-on-google-play based on decisions. ** move to next month's agenda
.. How and when should community members propose agenda items? The bot alerts a week before the meeting in the #fdroid-dev chat, but the agenda items were accidentally removed this time.
... Long-term: We should set up a forum where community members can propose agenda items and contribute asynchronously.
... Short-term: The solution of adding agenda items to the pad has mostly worked baring user error and allows us to have an option while we set up better infrastructure
.. The original 2-month period for the temporary measure in https://gitlab.com/fdroid/admin/-/issues/401 is almost over; it seems we need to extend it a bit longer though, since the Community Council is not in place yet. (extended)
.. Should we send an email every time there is an official vote in a meeting where all board members aren't present? (Morgan)
.. Set up a shared document (pad with -keep) for 'board onboarding' as a first step to new board elections. -- John will do Talk about new board elections next meeting
